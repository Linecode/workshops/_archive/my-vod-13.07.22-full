using Microsoft.EntityFrameworkCore;
using MyVod.Common.BuildingBlocks.EFCore;
using MyVod.Domain.Marketing.Domain;

namespace MyVod.Infrastructure.Marketing;

public class SubscriptionTypeDbContext : DbContext, IUnitOfWork
{
    public DbSet<SubscriptionType> SubscriptionTypes { get; private set; }
    
    public SubscriptionTypeDbContext(DbContextOptions<SubscriptionTypeDbContext> options) : base(options)
    {
        
    }
    
    public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
    {
        await SaveChangesAsync(cancellationToken);
        
        return true;
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<SubscriptionType>()
            .HasData(
                new SubscriptionType { Id = new Guid("4BCB5575-A3B0-48CD-9199-BEEC567433CC"), Price = 100_00, TimeSpan = TimeSpan.FromDays(30)},
                new SubscriptionType { Id = new Guid("9ABD91DA-9CAC-4B4B-9A8F-2ECB3FD01090"), Price = 550_00, TimeSpan = TimeSpan.FromDays(180)},
                new SubscriptionType { Id = new Guid("37DC0EAF-3A34-49A2-8208-E67539FD078E"), Price = 800_00, TimeSpan = TimeSpan.FromDays(360)}
            );
    }
}