using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyVod.Domain.Legal;
using MyVod.Domain.Legal.Domain;
using MyVod.Domain.SharedKernel;

namespace MyVod.Infrastructure.Legal;

public class LicenseEntityTypeConfiguration : IEntityTypeConfiguration<License>
{
    public void Configure(EntityTypeBuilder<License> builder)
    {
        builder.HasKey(x => x.Id);

        builder.Property(x => x.Id)
            .HasConversion(x => x.Value, s => new LicenseId(s));

        builder.Property(x => x.MovieId)
            .HasConversion(x => x.Value, s => new MovieId(s));

        builder.Property(x => x.Status)
            .HasConversion<string>();
        
        builder.OwnsOne(x => x.DateRange, dr =>
        {
            dr.Property(x => x.StartDate)
                .HasColumnName("StartDate");

            dr.Property(x => x.EndDate)
                .HasColumnName("EndDate");
        });

        builder.OwnsOne(x => x.RegionIdentifier, ri =>
        {
            ri.Property(x => x.Value)
                .HasColumnName("RegionIdentifier");
        });
        
        builder.ToTable("Licenses");
    }
}