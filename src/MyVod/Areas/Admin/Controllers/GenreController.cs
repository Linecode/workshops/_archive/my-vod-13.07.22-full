using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyVod.Infrastructure.Models;
using MyVod.Services;

namespace MyVod.Areas.Admin.Controllers;

[Authorize]
[Area("admin")]
public class GenreController : Controller
{
    private readonly IGenreService _genreService;

    public GenreController(IGenreService genreService)
    {
        _genreService = genreService;
    }

    [HttpGet]
    public async Task<IActionResult> Index()
    {
        var genres = _genreService.Get();

        return View("Index", genres.AsEnumerable());
    }

    [HttpGet]
    [ActionName("create")]
    public IActionResult CreateView()
    {
        return View("Create");
    }

    [HttpPost]
    [ActionName("create")]
    public async Task<IActionResult> Create(Genre genre)
    {
        if (ModelState.IsValid)
        {
            await _genreService.Create(genre);
            
            return RedirectToAction(nameof(Index));
        }

        return BadRequest();
    }

    [HttpGet("{area}/{controller}/{action}/{id:guid}")]
    [ActionName("edit")]
    public async Task<IActionResult> EditView(Guid id)
    {
        var genre = await _genreService.Get(id);

        if (genre is null)
            return NotFound();

        return View("Edit", genre);
    }

    [HttpPost("{area}/{controller}/{action}/{id:guid}")]
    [ActionName("edit")]
    public async Task<IActionResult> Edit(Guid id, Genre genre)
    {
        if (id != genre.Id)
            return BadRequest();
            
        if (ModelState.IsValid)
        {
            await _genreService.Update(genre);
            
            return RedirectToAction(nameof(Index));
        }

        return BadRequest();
    }

    [HttpGet("{area}/{controller}/{action}/{id:guid}/delete")]
    [ActionName("delete")]
    public async Task<IActionResult> Delete(Guid id)
    {
        if (ModelState.IsValid)
        {
            await _genreService.Delete(id);
            
            return RedirectToAction(nameof(Index));
        }

        return BadRequest();
    }
}