using MediatR;
using MyVod.Common.BuildingBlocks.Common;
using MyVod.Domain.CannonicalModel.PublishedLanguage;
using MyVod.Domain.Sales.B2b.Subscription.Application.Ports;

namespace MyVod.Domain.Sales.B2b.Subscription.Application.Commands;

public record NotifyUserCommand(UserContext UserContext) : IRequest<Result>;

internal class NotifyUserHandler : IRequestHandler<NotifyUserCommand, Result>
{
    private readonly IExternalNotificationService _externalNotificationService;

    public NotifyUserHandler(IExternalNotificationService externalNotificationService)
    {
        _externalNotificationService = externalNotificationService;
    }
    
    public async Task<Result> Handle(NotifyUserCommand request, CancellationToken cancellationToken)
    {
        await _externalNotificationService.SendNotification(request.UserContext.Email, "Subscription Purchased");
        
        return Result.Success();
    }
}