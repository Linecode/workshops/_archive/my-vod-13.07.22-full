using System.Linq.Expressions;
using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain.Legal.Domain;

public class AcquiredLicenseSpecification : Specification<License>
{
    public override Expression<Func<License, bool>> ToExpression()
        => license => license.DateRange.StartDate <= DateTime.UtcNow && license.DateRange.EndDate >= DateTime.UtcNow;
}