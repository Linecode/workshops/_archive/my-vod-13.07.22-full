using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Domain.Legal.Domain;
using MyVod.Domain.Legal.Infrastructure;
using MyVod.Domain.Legal.ReadModel;
using MyVod.Domain.MoviesCatalog.Domain;
using MyVod.Domain.MoviesCatalog.Infrastructure;
using MyVod.Domain.SharedKernel;

namespace MyVod.Domain.Legal.Application;

public interface ILicenseService : IApplicationService
{
    Task Save(DateRange dateRange, RegionIdentifier regionIdentifier, MovieId movieId);
    Task<LicenseReadModel?> Get(LicenseId licenseId);
    Task<IEnumerable<License?>> GetPassed();
    Task<IEnumerable<License?>> GetAcquired();
    Task Activate(LicenseId id);
    Task DeActivate(LicenseId id);
}

public class LicenseService : ILicenseService
{
    private readonly ILicenseRepository _repository;
    private readonly IMovieRepository _movieRepository;

    public LicenseService(ILicenseRepository repository, IMovieRepository movieRepository)
    {
        _repository = repository;
        _movieRepository = movieRepository;
    }
    
    public async Task Save(DateRange dateRange, RegionIdentifier regionIdentifier, MovieId movieId)
    {
        var license = new License(dateRange, regionIdentifier, movieId);
        
        _repository.Add(license);
        await _repository.UnitOfWork.SaveEntitiesAsync();
    }

    public async Task<LicenseReadModel?> Get(LicenseId licenseId)
    {
        var license  = await _repository.Get(licenseId);

        if (license is null)
            return null;
        
        var movie = await _movieRepository.Get(license.MovieId);

        if (movie is null)
            throw new Exception();

        return LicenseReadModel.From(license, movie.Title.English);
    }

    public async Task<IEnumerable<License?>> GetPassed()
    {
        var spec = new PassedLicenseSpecification();

        var licenses = await _repository.Get(spec);

        return licenses.AsEnumerable();
    }

    public async Task<IEnumerable<License?>> GetAcquired()
    {
        var spec = new AcquiredLicenseSpecification().And(new NotActiveLicenseSpecification());

        var licenses = await _repository.Get(spec);

        return licenses.AsEnumerable();
    }

    public async Task Activate(LicenseId id)
    {
        var license = await _repository.Get(id);
        
        license.Activate();

        await _repository.UnitOfWork.SaveEntitiesAsync();
    }

    public async Task DeActivate(LicenseId id)
    {
        var license = await _repository.Get(id);
        
        license.DeActivate();

        await _repository.UnitOfWork.SaveEntitiesAsync();
    }
}