using MediatR;
using MyVod.Domain.CannonicalModel.Events;
using MyVod.Domain.MoviesCatalog.Domain;

namespace MyVod.Domain.MoviesCatalog.Application.Listeners;

public class LicenseExpiredEventListener : INotificationHandler<LicenseExpiredEvent>
{
    private readonly IMovieService _movieService;

    public LicenseExpiredEventListener(IMovieService movieService)
    {
        _movieService = movieService;
    }
    
    public async Task Handle(LicenseExpiredEvent notification, CancellationToken cancellationToken)
    {
        await _movieService.UnPublish(notification.MovieId);
    }
}
