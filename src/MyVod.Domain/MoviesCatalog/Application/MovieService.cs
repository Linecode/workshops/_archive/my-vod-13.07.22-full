using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Domain.MoviesCatalog.Domain;
using MyVod.Domain.MoviesCatalog.Domain.Policies.Publish;
using MyVod.Domain.MoviesCatalog.Domain.Specifications;
using MyVod.Domain.MoviesCatalog.Infrastructure;
using MyVod.Domain.MoviesCatalog.ReadModel;
using MyVod.Domain.SharedKernel;

namespace MyVod.Domain.MoviesCatalog.Application;

public interface IMovieService : IApplicationService
{
    Task<Movie> Get(MovieId id);
    Task<IEnumerable<MovieReadModel>> GetAvailableMovies(RegionIdentifier regionIdentifier);
    Task ChangeMetadata(MovieId id, string origin, DateTime? releasedAt);
    Task UnPublish(MovieId id);
    Task Publish(MovieId id, RegionIdentifier regionIdentifier);
}

public class MovieService : IMovieService
{
    private readonly IMovieRepository _movieRepository;

    public MovieService(IMovieRepository movieRepository)
    {
        _movieRepository = movieRepository;
    }
    
    public async Task<Movie?> Get(MovieId id)
    {
        return await _movieRepository.Get(id);
    }

    public async Task ChangeMetadata(MovieId id, string origin, DateTime? releasedAt)
    {
        var movie = await _movieRepository.Get(id);

        if (movie is null)
            return;

        var metadata = new MetaData(origin, releasedAt);
        movie.AddMetaData(metadata);

        await _movieRepository.UnitOfWork.SaveEntitiesAsync();
    }

    public async Task UnPublish(MovieId id)
    {
        var movie = await _movieRepository.Get(id);

        if (movie is null)
            return;
        
        movie.UnPublish();

        await _movieRepository.UnitOfWork.SaveEntitiesAsync();
    }

    public async Task Publish(MovieId id, RegionIdentifier regionIdentifier)
    {
        var movie = await _movieRepository.Get(id);

        if (movie is null)
            return;
        
        var policy = PublishPolicies.Default;
        movie.Publish(regionIdentifier, policy);

        await _movieRepository.UnitOfWork.SaveEntitiesAsync();
    }

    public async Task<IEnumerable<MovieReadModel>> GetAvailableMovies(RegionIdentifier regionIdentifier)
    {
        var spec = new PublishedMovieSpecification() & new MovieRegionSpecification(regionIdentifier);

        var movies = await _movieRepository.GetReadModel(spec);
        
        return movies!;
    }
}