using System.Linq.Expressions;
using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain.MoviesCatalog.Domain.Specifications;

public class PublishedMovieSpecification : Specification<Movie>
{
    public override Expression<Func<Movie, bool>> ToExpression()
        => movie => movie.Status == Movie.MovieStatus.Published;
}