using MyVod.Common.BuildingBlocks.Common;
using MyVod.Domain.SharedKernel;

namespace MyVod.Domain.MoviesCatalog.Domain.Policies.Publish;

public static class PublishPolicies
{
    public static PublishPolicy Default => new CompositePublishCheck(new RegionCheck(), new PriceCheck());
    
    private class RegionCheck : PublishPolicy
    {
        public Result Publish(Movie movie)
            => movie.RegionIdentifier != RegionIdentifier.Missing ? Result.Success() : Result.Error(new MissingRegionException());
    }

    private class PriceCheck : PublishPolicy
    {
        public Result Publish(Movie movie)
            => movie.HasPrice ? Result.Success() : Result.Error(new MissingPriceException());
    }
    
    private class AlwaysTruePolicy : PublishPolicy
    {
        public Result Publish(Movie movie)
            => Result.Success();
    }

    private class CompositePublishCheck : PublishPolicy
    {
        private readonly PublishPolicy[] _publishPolicies;

        public CompositePublishCheck(params PublishPolicy[] publishPolicies)
        {
            _publishPolicies = publishPolicies;
        }

        public Result Publish(Movie movie)
            => _publishPolicies.Select(x => x.Publish(movie))
                .Where(x => !x.IsSuccessful)
                .DefaultIfEmpty(Result.Success())
                .First();
    }
}