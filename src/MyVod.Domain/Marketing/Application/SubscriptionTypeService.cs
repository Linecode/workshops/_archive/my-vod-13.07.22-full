using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Domain.Marketing.Domain;
using MyVod.Domain.Marketing.Infrastructure;

// PL -> Published Language
using PLSubscriptionType = MyVod.Domain.CannonicalModel.PublishedLanguage.SubscriptionType;

namespace MyVod.Domain.Marketing.Application;

public interface ISubscriptionTypeService : IApplicationService
{
    Task Create(uint price, TimeSpan timeSpan);
    Task<IEnumerable<PLSubscriptionType>> Get();
    public Task<PLSubscriptionType?> Get(Guid id); 
}

public class SubscriptionTypeService : ISubscriptionTypeService
{
    private readonly ISubscriptionTypeRepository _repository;

    public SubscriptionTypeService(ISubscriptionTypeRepository repository)
    {
        _repository = repository;
    }

    public async Task Create(uint price, TimeSpan timeSpan)
    {
        var subscriptionType = new SubscriptionType
        {
            Id = Guid.NewGuid(),
            Price = price,
            TimeSpan = timeSpan
        };

        _repository.Add(subscriptionType);
        await _repository.UnitOfWork.SaveChangesAsync();
    }

    public async Task<IEnumerable<PLSubscriptionType>> Get()
    {
        var subscriptionTypes = await _repository.Get();

        return subscriptionTypes.Select(Mapper.From).AsEnumerable();
    }

    public async Task<PLSubscriptionType?> Get(Guid id)
    {
        var subscriptionType = await _repository.Get(id);

        if (subscriptionType is null)
            return null;

        return Mapper.From(subscriptionType);
    }

    private static class Mapper
    {
        public static PLSubscriptionType From(SubscriptionType st)
            => PLSubscriptionType.Create(st.Id, (int)st.Price, st.TimeSpan);
    }
}