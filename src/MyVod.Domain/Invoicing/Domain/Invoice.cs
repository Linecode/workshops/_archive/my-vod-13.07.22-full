using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain.Invoicing.Domain;

public sealed class Invoice : Entity<InvoiceId>, AggregateRoot<InvoiceId>
{
    public DateTime CreatedAt { get; private set; } = DateTime.UtcNow;

    public InvoiceParty From { get; private set; }

    public InvoiceParty To { get; private set; }

    public DateTime DueDate { get; private set; }

    public InvoiceStatus Status { get; private set; } = InvoiceStatus.Active;

    private readonly List<InvoiceLine> _lines = new();

    public IReadOnlyList<InvoiceLine> Lines => _lines;

    public Invoice(InvoiceParty from, InvoiceParty to, DateTime dueDate, IEnumerable<InvoiceLine> lines)
    {
        From = from;
        To = to;
        DueDate = dueDate;

        _lines = lines.ToList();
        
        Id = InvoiceId.New();
    }
}

// Once again simplification
public enum InvoiceStatus
{
     Active,
     Overdue,
}