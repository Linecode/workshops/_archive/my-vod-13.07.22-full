using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain.Invoicing.Domain;

// It's simplified
public class InvoiceParty : ValueObject<InvoiceParty>
{
    public string Name { get; private set; }
    public string Address { get; private set; }
    public string TaxId { get; private set; }

    public InvoiceParty(string name, string address, string taxId)
    {
        Name = name;
        Address = address;
        TaxId = taxId;
    }

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Name;
        yield return Address;
        yield return TaxId;
    }
}