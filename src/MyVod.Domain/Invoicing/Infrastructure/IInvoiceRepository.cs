using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Domain.Invoicing.Domain;

namespace MyVod.Domain.Invoicing.Infrastructure;

public interface IInvoiceRepository : IRepository
{
    Task Add(Invoice invoice);
    Task<Invoice> Get(InvoiceId id);
}