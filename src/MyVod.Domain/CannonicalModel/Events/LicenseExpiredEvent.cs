using MediatR;
using MyVod.Domain.Legal.Domain;
using MyVod.Domain.SharedKernel;

namespace MyVod.Domain.CannonicalModel.Events;

public class LicenseExpiredEvent : INotification
{
    public LicenseId LicenseId { get; }
    public MovieId MovieId { get; }

    public LicenseExpiredEvent(LicenseId licenseId, MovieId movieId)
    {
        LicenseId = licenseId;
        MovieId = movieId;
    }
}