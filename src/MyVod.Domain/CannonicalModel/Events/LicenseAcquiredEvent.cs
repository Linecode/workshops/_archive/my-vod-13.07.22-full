using MediatR;
using MyVod.Domain.Legal.Domain;
using MyVod.Domain.SharedKernel;

namespace MyVod.Domain.CannonicalModel.Events;

public class LicenseAcquiredEvent : INotification
{
    public DateRange DateRange { get; }
    public LicenseId LicenseId { get; }
    public MovieId MovieId { get; }
    public RegionIdentifier RegionIdentifier { get; }

    public LicenseAcquiredEvent(DateRange dateRange, LicenseId licenseId, MovieId movieId, RegionIdentifier regionIdentifier)
    {
        DateRange = dateRange;
        LicenseId = licenseId;
        MovieId = movieId;
        RegionIdentifier = regionIdentifier;
    }
}