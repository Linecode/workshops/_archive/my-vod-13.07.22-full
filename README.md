# MyVod - DDD Workshop

Bardziej rozbudowana domena warsztatowa

## Subdomeny

| Nazwa        | Opis | Wzorce |
|--------------| --- |--------|
| Legal        | Subdomena odpowiedzialna za zarządzanie cyklem życia licencji | DDD    |
| Marketing    | Subdomena odpowiedzialna za zarządzanie rodzajami subskrypcji | CRUD   |
| MovieCatalog | Subdomena odpowiedzialna za zarządzanie katalogiem filmów | DDD |
| Sales/B2B    | Subdomena odpowiedzialna za zakup subskrypcji | DDD / CQRS / Event Sourcing | 

### Relacje

Legal --> Published Language / Shared Kernel --> MoviesCatalog

Marketing --> Customer - Supplier --> Sales

### Sagii

Procesy długotrwające, przechodzące krokowo przez różne aggregaty / subdomeny

| Nazwa | Opis |
| --- |------|
| LicenseAquireSaga | Saga uruchamiana po aktywacji licencji |
| CreateSubscriptionSaga | Saga uruchamiana aby zakończyć płatność i utworzyć subskrypcje dla uzytkownika |


## Domena Sales / b2b - słowo końcowe

Do implementacji zachowań tej domeny moglibyśmy użyć nie sagi a struktur dużej skali
wtedy naszym zasobem (potencjałem) byłby agregat Subscription następnie agregaty Order i Purchase byłyby
operacjami (uprościło by to agregat Order).

## Uruchomienie procesu zakupu subskrypcji

Aby uruchomic proces subskrypcji należy przejść na stronę Privacy w górnym menu. 
Jako że Panowie nie bracują w aplikacjach Web to nie tworzyłem kolejnych widoków aby nie komplikować tej warstwy.
